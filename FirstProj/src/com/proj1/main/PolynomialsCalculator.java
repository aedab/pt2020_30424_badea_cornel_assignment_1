
package com.proj1.main;

import com.proj1.controller.CalculatorController;
import com.proj1.model.*;
import com.proj1.view.*;

/**
 * PolynomialClaculator.java - A Calculator of polynomials implemented in JAVA
 *
 * @author: Cornel Alexandru Badea
 * @version: 0.0.1
 * @date: 03/15/20
 *
 *
 * Description: This program is a Calculator of Polynomials capable of performing
 * addition, subtraction, multiplication, division, derivative, integration
 * of polynomials with integer coefficient and power and single variable.
 * * **/
public class PolynomialsCalculator {
    public static void main(String[] args){

        View1 theView = new View1();


        CalculatorModel theModel = new CalculatorModel();

        CalculatorController theController = new CalculatorController((ViewI) theView, theModel);


    }
}
