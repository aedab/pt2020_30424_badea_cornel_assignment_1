package com.proj1.model;

/*Monomial class has variable exponent coeficient attributes
 * also specific funtions used in operations with polynomials
 * the variable can be a letter or a space ' ' for convenience when printing
 */
public class Monomial {
    private char variable;
    private int exponent;
    private int coeficient;
    private Fraction fraction;
    private boolean isFrac;

    public Monomial(char variable, int exponent, int coeficient) {

        this.variable = variable;
        this.exponent = exponent;
        this.coeficient = coeficient;
        isFrac = false;
    }


    public char getVariable() {
        return variable;
    }

    public int getExponent() {
        return exponent;
    }

    public int getCoeficient() {
        return coeficient;
    }

    public Fraction getFraction() {
        return fraction;
    }

    public void setVariable(char variable) {
        this.variable = variable;
    }

    public void setExponent(int exponent) {
        this.exponent = exponent;
    }

    public void setCoeficient(int coeficient) {
        this.coeficient = coeficient;
    }

    public void setFraction(Fraction fraction) {
        this.isFrac = true;
        this.fraction = fraction;
    }
    public boolean checkFraction(){return isFrac;}

    public void addCoeficient(int coeficient) {
        this.coeficient += coeficient;
    }
    /*
    public void printMonomial(){

        //print monomial
        System.out.print(((coeficient>0)?"+":"")+coeficient);
        System.out.print(variable);
        if(exponent != 0)
            System.out.print("^"+exponent);


    }*/

    /**
     * stringMonomial method converts a Monomial object to it's corespondent string
     * @return String
     * */

    public String stringMonomial(){
        String stringMonomial = "";
        //print monomial
        if(isFrac == false) {
            stringMonomial += ((coeficient > 0) ? "+" : "") + coeficient;
        }else{
            stringMonomial += ((this.fraction.getNumerator() > 0) ? "+" : "") +  ((this.fraction.getDenominator() != 1)?this.fraction.toString():this.fraction.getNumerator());
        }
        stringMonomial += variable;
        if(exponent != 0)
        {    stringMonomial += "^";
            stringMonomial += exponent;
        }
        return stringMonomial;

    }
    /**
     * divideMonno method This method is use to divide two monomials
     * @param anotherMono This is the divider monomial
     * @return Monomial The returned value is a Monomial object, result of a division
     * */
    public Monomial divideMono(Monomial anotherMono) throws Exception{
        Monomial monoTemp;
        Fraction fraction = this.fraction.divide(anotherMono.getFraction());
        int power = this.getExponent() - anotherMono.getExponent();
        monoTemp = new Monomial(((power == 0)?' ':this.getVariable()),// if the power becames 0 change the variable
                power,
                1);
        monoTemp.setFraction(fraction);

        return monoTemp;
    }
    /**
     * method:integrateMono() This method integrates the current monomial
     * according to the mathematical rules of integration*/
    public Monomial integrateMono(){
        if(this.variable != ' ') {
            isFrac = true;
            this.fraction = new Fraction(this.getCoeficient(), this.getExponent()+1);
            this.fraction = this.fraction.reduce();
            this.setExponent(this.getExponent() + 1);
        }
        else{
            this.variable = 'x';
            this.setExponent(1);
        }
        return this;

    }
    /**
     * makeFraction() method This method attibutes a value to the this.fraction variable
     * corespondent to the current coeficient divided by 1*/
    public void makeFraction(){
        this.isFrac = true;
        this.fraction = new Fraction(this.coeficient, 1);
    }
}
