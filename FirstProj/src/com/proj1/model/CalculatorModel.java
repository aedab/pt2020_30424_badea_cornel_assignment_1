package com.proj1.model;


import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * Calculator Model class - the model in the MVC Pattern
 * It contains all the necessary methods for validating and processing the input
 * introduces by the user by the view
 * Also containing the necessary methods performing the desired computations
 * on previously introduced inputs
 *
 */

public class CalculatorModel {
    //the maximum number of characters a polynomial represented as a string
    //can have in order to be valid
    private static final int MAX_LENGHT_POLYNOMIAL = 9999999;

    /**
     * @param string This is the string that must be converted into a Monomial type object
     * @return Monomial This is the actual Monomial object obtained
     * */
    private static Monomial makeMonomial(String string){
        Monomial monomial;
        //default coeficient be 1
        int coef = 1;
        //default power is 0
        int power = 0;
        // default variable character is ' ' - coresponing to power 0
        char variable = ' ';
        Pattern p;
        Matcher m;

        //use regex to identify the variable letter and use the specific pattern
        p = Pattern.compile("([a-z])");
        m = p.matcher(string);
        if (m.find()) {//if we found a match with by our regex
            //store the variable found
            variable = m.group(1).charAt(0);
            //also get the power with the regex \\^\\d+
            p = Pattern.compile("(\\^\\d+)");
            m = p.matcher(string);
            if (m.find()) {
                //if we can read the power then store it
                power = Integer.parseInt(m.group(1).substring(1));
            }else{power = 1;//else we asume it is 1 since it does not appear
            }

        }

        //now we extract the coeficient
        String coefString = string;
        if(variable != ' ') {
            //if we do not have a ' '-type variable the get the substring
            //util the appearance of that varable
            coefString = string.substring(0, string.indexOf(variable));

        }
        //if the coeficient does not appear it means it remains 1
        if(!coefString.equals("")){
            if(coefString.equals("-")) // a minus appears so -1 coeficient
                coef = -1;
            else if(coefString.equals("+")){ // a plus appears so  1 coeficient
                coef = 1;
            }
            else
                coef = Integer.parseInt(coefString);
        }

        monomial = new Monomial(variable, power, coef);
        return monomial;

    };

    /**
     * substractPolynomials - method to compute the substraction between two polynomials
     * @param str1 This is the polynomial that is substracted from
     * @param str2 This is the polynomial that will be substracted
     * @return String This is the result polynomial
     * */
    public static String substractPolynomials(String str1, String str2) throws Exception{
        String resultString;
        Polynomial result;

        Polynomial[] polynomials;

        polynomials = stringToPolynomial(str1, str2);
        polynomials[0].sortPoly();
        polynomials[1].sortPoly();
        result = polynomials[0].substractPoly(polynomials[1]);
        result.compress();
        result.sortPoly();
        resultString = result.stringPoly();

        return resultString;
    }

    /**
     * integratePolynomials - method to compute the integrate of a  polynomial
     * @param str1 This is the polynomial that is integrated
     * @return String This is the result polynomial
     * */
    public static String integratePolynomial(String str1) throws Exception{
        String resultString;


        Polynomial[] polynomials;

        polynomials = stringToPolynomial(str1, "x");
        polynomials[0].sortPoly();

        polynomials[0].integratePoly();

        resultString = polynomials[0].stringPoly();
        return resultString;
    }

    /**
     * derivatePolynomials - method to compute the derivateve of a  polynomial
     * @param str1 This is the polynomial that is derivated
     * @return String This is the result polynomial
     * */
    public static String derivatePolynomial(String str1) throws Exception{
        String resultString;


        Polynomial[] polynomials;

        polynomials = stringToPolynomial(str1, "x");
        polynomials[0].sortPoly();

        polynomials[0].derivatePoly();

        resultString = polynomials[0].stringPoly();
        return resultString;
    }

    /**
     * addPolynomials - method to compute the addition between two polynomials
     * @param str1 This is first polynomial to be added
     * @param str2 This is second polynomial to be added
     * @return String This is the sum result polynomial
     * */

    public static String addPolynomials(String str1, String str2) throws Exception{
        String resultString;
        Polynomial result;

        Polynomial[] polynomials;

        polynomials = stringToPolynomial(str1, str2);
        polynomials[0].sortPoly();
        polynomials[1].sortPoly();
        result = polynomials[0].addPoly(polynomials[1]);
        result.compress();
        result.sortPoly();
        resultString = result.stringPoly();
        return resultString;
    }
    /**
     * multiplyPolynomials - method to compute the multiplication between two polynomials
     * @param str1 This is the first polynomial that is multiplied
     * @param str2 This is the second polynomial that will be multiplied
     * @return String This is the result polynomial
     * */
    public static String multiplyPolynomials(String str1, String str2) throws Exception{
        String resultString;
        Polynomial result;

        Polynomial[] polynomials;

        polynomials = stringToPolynomial(str1, str2);
        polynomials[0].sortPoly();
        polynomials[1].sortPoly();
        result = polynomials[0].multiplyPoly(polynomials[1]);
        result.compress();
        result.sortPoly();
        resultString = result.stringPoly();
        return resultString;
    }

    /**
     * dividePolynomials - method to compute the division between two polynomials
     * @param str1 This is the divided polynomial
     * @param str2 This is the divisor polynomial
     * @return String This is the reminder polynomial appended at the quotient polynomial
     * */
    public static String dividePolynomials(String str1, String str2) throws Exception{
        String resultStringQuotient = "Quotient:";
        String resultStringReminder = " | Reminder:";

        Polynomial[] result;

        Polynomial[] polynomials;

        polynomials = stringToPolynomial(str1, str2);
        polynomials[0].sortPoly();
        polynomials[1].sortPoly();
        result = polynomials[0].dividePoly(polynomials[1]);
        result[0].sortPoly();
        result[1].sortPoly();
        resultStringQuotient += result[0].stringPoly();
        resultStringReminder += result[1].stringPoly();

        return (resultStringQuotient+resultStringReminder);
    }

    /**
     * stringToPolynomial This method converts two string into two polynomials
     * @param str2 This is one polynomial string to be converted
     * @param str1 This is one polynomial string to be converted
     * @return Polynomial[] This is an array of two Polynomial type objects
     * @throws Exception This is the thrown exception if the input strings are not in required polynomial format
     * */
    static private Polynomial[] stringToPolynomial(String str1, String str2) throws Exception{
        //if the lenght of any polynomial is greater than the maximum allowed value throw an error
        if(str1.length() > MAX_LENGHT_POLYNOMIAL || str2.length() > MAX_LENGHT_POLYNOMIAL)
            throw new Exception("The polynomial lenght exceeded!");

        Polynomial[] result = new Polynomial[2];
        Polynomial poly1 = new Polynomial();
        Polynomial poly2 = new Polynomial();
        Monomial mono;
        String exp1 = str1;
        String exp2 = str2;
        //we use a regex pattern to delimit the present monomials
        Pattern pattern = Pattern.compile("([+-]?[^-+]+)");
        Matcher matcher1 = pattern.matcher(exp1);
        Matcher matcher2 = pattern.matcher(exp2);
        while (matcher1.find()) {//while we found some monomials
            try {
                //try computing the instantiation of that particular monomial
                mono = makeMonomial(matcher1.group(1));
                //add it to the list of momnomial in the first polynomial
                poly1.listMonomials.add(mono);
            }
            catch(Exception e){//if an exception occurs print message in backend and throw it
                System.out.print("\nError durin reading polynomial\n");
                throw e;
            }

        }

        while (matcher2.find()) {


            try {
                //try computing the instantiation of that particular monomial
                mono = makeMonomial(matcher2.group(1));
                //add it to the list of momnomial in the second polynomial
                poly2.listMonomials.add(mono);
            }
            catch(Exception e){//if an exception occurs print message in backend and throw it
                System.out.print("\nError durin reading polynomial\n");
                throw e;
            }

        }
        //store the results in an array and return it
        result[0] = poly1;
        result[1] = poly2;
        return result;
    }



}
