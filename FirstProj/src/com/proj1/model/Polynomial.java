package com.proj1.model;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;

public class Polynomial {

    //linked list for the monomials
    LinkedList<Monomial> listMonomials = new LinkedList<Monomial>();
    /*
    //function to print the polynomial in the order or iterating in linked list
    public void printPoly(){
        for (Monomial mono: this.listMonomials) {
            if (mono != null)
                mono.printMonomial();
        }
        System.out.print("\n");
    }*/
    /**
     * stringPoly method This method converts a Polynomial type object into it's corresponding string
     * @return String the computed string
     * */
    public String stringPoly(){
        String result = "";
        for (Monomial mono: this.listMonomials) {
            if (mono != null)
                result += mono.stringMonomial();
        }

        return result;

    }

    /**
     * addPoly method This method adds polynomials represented in the linked lists
     * of two polynomial objects
     * @param poly2 This is the the polynomial added to the current one
     * */

    public Polynomial addPoly(Polynomial poly2){
        Polynomial result = this;

        for (Monomial mono2:poly2.listMonomials) {
            boolean found = false;
            for (Monomial mono:result.listMonomials) {

                //if found a matching variable add the coeficient to it
                if(mono.getVariable() == mono2.getVariable() && mono.getExponent() == mono2.getExponent()){
                    found = true;

                    mono.addCoeficient(
                            mono2.getCoeficient()
                    );
                    if(mono.getCoeficient() == 0)
                        listMonomials.remove(mono);
                    break;

                }
            }
            //if we did not found any matching variables for out monomial just add it
            if(found == false){
                result.listMonomials.add(mono2);
            }
        }

        return result;

    }
    /**
     * substractFractionPoly method This method is used as an auxiliary function for the divide method that
     * requires a substraction between monomials that have fractions as coeficients
     * @param poly2 used as the polynomial substracted from the one that invoked the method
     * @return result the resulted polynomial
     * */
    public Polynomial substractFractionPoly(Polynomial poly2)throws Exception{
        Polynomial result = this;

        for (Monomial mono2:poly2.listMonomials) {
            boolean found = false;
            for (Monomial mono:result.listMonomials) {


                if(mono.getVariable() == mono2.getVariable() && mono.getExponent() == mono2.getExponent()){
                    found = true;
                    mono.setFraction(mono.getFraction().subtract(mono2.getFraction()));
                    if(mono.getFraction().getNumerator()== 0)
                        listMonomials.remove(mono);
                    break;

                }
            }
            //if we did not found any matching variables for out monomial just add it negated
            if(found == false){
                Fraction fraction = mono2.getFraction();
                fraction.setNumerator(-mono2.getFraction().getNumerator());
                mono2.setFraction(fraction);
                result.listMonomials.add(mono2);
            }
        }

        return result;

    }
    /**
     * substractPoly method This method substracts polynomials represented in the linked lists
     * of two polynomial objects
     * @param poly2 This is the the polynomial subtracted from the current one
     * */

    public Polynomial substractPoly(Polynomial poly2){
        Polynomial result = this;

        for (Monomial mono2:poly2.listMonomials) {
            boolean found = false;
            for (Monomial mono:result.listMonomials) {

                //if found a matching variable add the coeficient to it
                if(mono.getVariable() == mono2.getVariable() && mono.getExponent() == mono2.getExponent()){
                    found = true;

                    mono.addCoeficient(
                            -mono2.getCoeficient()
                    );
                    if(mono.getCoeficient() == 0)
                        listMonomials.remove(mono);
                    break;

                }
            }
            //if we did not found any matching variables for out monomial just add it
            if(found == false){
                mono2.setCoeficient(-mono2.getCoeficient());
                result.listMonomials.add(mono2);
            }
        }

        return result;

    }
    // function to add or substract the monomials with the same power in a polynomial
    public void compress(){
        Iterator<Monomial> monoIterator = listMonomials.iterator();
        while(monoIterator.hasNext()) {
            Monomial temp = monoIterator.next();
            for (int i = listMonomials.indexOf(temp) + 1; i < listMonomials.size(); i++) {
                if(temp.getVariable() == listMonomials.get(i).getVariable() &&
                        temp.getExponent() == listMonomials.get(i).getExponent()){
                    listMonomials.get(i).addCoeficient(temp.getCoeficient());
                    monoIterator.remove();

                    break;
                }
            }
        }
        for (Iterator<Monomial> iterator = listMonomials.iterator(); iterator.hasNext();) {
            Monomial mono= iterator.next();
            if(mono.getCoeficient() == 0) {
                iterator.remove();
            }
        }
    }


    /**
     * method: mutiplyPoly This method is use for elementwise multiplication
     * of the elements of the polynomials and also compression of the resulted
     * polynomial so that every monomial has a unique power
     * @param poly2  this is the polynomial that is multiplied with the current one
     * @return The value returned is a Polynomial object called result
     * */
    public Polynomial multiplyPoly(Polynomial poly2) throws Exception {

        Polynomial result = new Polynomial();
        Monomial monoTemp;
        for (Monomial mono1:listMonomials) {
            for (Monomial mono2:poly2.listMonomials) {
                if(mono1.checkFraction() == false ) {
                    monoTemp = new Monomial(((mono1.getVariable() == ' ') ? mono2.getVariable() : mono1.getVariable()),
                            mono1.getExponent()+mono2.getExponent(),
                            mono1.getCoeficient()*mono2.getCoeficient());
                    result.listMonomials.add(monoTemp);
                }
                else{
                    Fraction fraction = mono1.getFraction().multiply(mono2.getFraction());

                    monoTemp = new Monomial(((mono1.getVariable() == ' ') ? mono2.getVariable() : mono1.getVariable()),
                            mono1.getExponent()+mono2.getExponent(),
                            1);
                    monoTemp.setFraction(fraction);
                    result.listMonomials.add(monoTemp);
                }
            }
        }
        result.compress();

        return result;
    }
    //function to negate a polynomial // no used
    /*public Polynomial negatePoly(){
        for (Monomial mono:this.listMonomials) {
            mono.setCoeficient(-mono.getCoeficient());
        }
        return this;
    };*/

    /**
     * dividePoly method This method divide one polynomial by another and gives the result
     * @param anotherPoly This is the divider Polynomial
     * @return 2 size array of polynomials containing the quotient and the reminder of the division
     * */
    public Polynomial[] dividePoly(Polynomial anotherPoly) throws Exception{
        for (Monomial mono: this.listMonomials) {
            mono.makeFraction();
        }
        for (Monomial mono: anotherPoly.listMonomials) {
            mono.makeFraction();
        }

        Polynomial[] quotientAndReminder = new Polynomial[2];
        Monomial monoTemp;
        Monomial mono2 = anotherPoly.listMonomials.getFirst();
        Polynomial polySubstractFrom = this;
        Polynomial polyMultiplier = new Polynomial();
        Polynomial polyToSubstract ;
        Polynomial finalQuotient = new Polynomial();

        while(!polySubstractFrom.listMonomials.isEmpty() && mono2.getExponent() <= polySubstractFrom.listMonomials.getFirst().getExponent()) {
            monoTemp = polySubstractFrom.listMonomials.getFirst().divideMono(mono2);

            finalQuotient.listMonomials.add(monoTemp);
            polyMultiplier.listMonomials.add(monoTemp);
            polyToSubstract = polyMultiplier.multiplyPoly(anotherPoly);
            polySubstractFrom.substractFractionPoly(polyToSubstract);

            polyMultiplier.listMonomials.remove(monoTemp);
        }
        quotientAndReminder[0] = finalQuotient;
        quotientAndReminder[1] = polySubstractFrom;

        return quotientAndReminder;
    }

    /**
     * method: derivatePoly() This method derivate the polynomial form
     * the Polynomial object that called the method
     * */
    public void derivatePoly() {

        for (Monomial mono : this.listMonomials) {
            if (mono.getExponent() != 0) {
                mono.setCoeficient(mono.getCoeficient() * mono.getExponent());
                mono.setExponent(mono.getExponent() - 1);
                if (mono.getExponent() == 0) {
                    mono.setVariable(' ');
                }
            } else {
                this.listMonomials.remove(mono);
            }
        }
    }

    /**
     * method: integratePoly() This method integrate the polynomial form
     * the Polynomial object that called the method
     * */
    public Polynomial integratePoly(){
        for (Monomial mono: this.listMonomials) {
            mono.integrateMono();
        }
        return this;
    }


    /**
     * method: sortPoly() This method sorts the polynomial from the Polynomial
     * object that calls the method
     * */
    public void sortPoly(){

        Collections.sort(this.listMonomials, new Comparator<Monomial>() {
            @Override
            public int compare(Monomial m1, Monomial m2) {
                return m2.getExponent() - m1.getExponent();
            } } );
    }
}
