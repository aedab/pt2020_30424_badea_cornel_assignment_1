package com.proj1.model;
/**
 * @class Fraction This class is used for operations with fractions
 * */
public class Fraction
{
   private int denominator;
   private int numerator;



   public Fraction()
   {
      this.numerator = 0;
      this.denominator = 0;
   }

   public Fraction(int numerator, int denumerator) {
      this.numerator = numerator;
      this.denominator = denumerator;
   }

   public int getNumerator()
   {
      return numerator;
   }


   public int getDenominator()
   {
      return denominator;
   }


   public void setNumerator(int numerator)
   {
      this.numerator = numerator;
   }


   public void setDenominator(int denumerator)
   {
      this.denominator = denumerator;
   }

   public String toString()
   {
      String string = numerator + "/" + denominator;
      return string;
   }

   /**
    * method substract This method is used for fraction substraction*/
   public Fraction subtract(Fraction frac2) throws Exception {
      Fraction lcmA, lcmB;
      //check if both denominators are valid
      if ((denominator == 0) || (frac2.denominator == 0))
         throw new Exception("Invalid substraction");
      //compute the least common multiplier for proper conversion
      int lcm = leastCommonMultiplier(denominator, frac2.denominator);
      //we set both denominators to the same value
      lcmA = prepare(lcm);
      lcmB = frac2.prepare(lcm);

      Fraction result = new Fraction();

      //we perform the actual substraction
      result.numerator = lcmA.numerator - lcmB.numerator;
      result.denominator = lcm;
      //we reduce the numerator with the denumerator
      result = result.reduce();
      return result;
   }

   /**
    *method multiply This method is use for multiplication between fractions
    * */
   public Fraction multiply(Fraction frac2) throws Exception {
      Fraction result = new Fraction();
      //check if we have valid denominators
      if (denominator == 0 || frac2.denominator == 0)
         throw new Exception("Invalid multiplication");
      //multiply
      result.numerator = numerator * frac2.numerator;
      result.denominator = denominator * frac2.denominator;

      result = result.reduce();
      return result;
   }

   /**
    *method divide This method is use for multiplication between fractions
    * */
   public Fraction divide(Fraction frac2) throws Exception {
      Fraction result = new Fraction();

      if (denominator == 0 || frac2.denominator == 0) {
         throw new Exception("Invalid division");

      }
      result.numerator = numerator * frac2.denominator;
      result.denominator = denominator * frac2.numerator;

      result = result.reduce();
      return result;
   }
   /**
    * method greatestCommonDiviser This method is used for computing the greatest common diviser*/
   private int greatestCommonDiviser(int a, int b)
   {
      int factor;
      while (b != 0) {
         factor = b;
         b = a % b;
         a = factor;
      }
      return a;
   }
   /**
    * method leastCommonMultiplier This method is used for computing the least common multiplier*/
   private int leastCommonMultiplier(int a, int b)
   {
      int factor = a;
      while ((a % b) != 0)
         a += factor;
      return a;
   }


   /**
    * method prepare This method is used to amplify the fraction in order for the denominator
    * to corespond to a least common diviser*/
   private Fraction prepare(int lcm)
   {
      Fraction result = new Fraction();
      int factor = lcm / denominator;
      result.numerator = numerator * factor;
      result.denominator = lcm;
      return result;
   }

   /**
    * method reduce This method is used to divide both the numerator and denominator according
    * to the gratest common divider*/
   public Fraction reduce()
   {
      Fraction result = new Fraction();
      int gcd;

      int num = Math.abs(numerator);
      int den = Math.abs(denominator);

      if(num > den){
         gcd = greatestCommonDiviser(num, den);
      }
      else if(num < den){
         gcd = greatestCommonDiviser(den, num);
      }else {
         gcd = num;
      }


      result.numerator = numerator / gcd;
      result.denominator = denominator / gcd;
      return result;
   }

}

