
package com.proj1.view;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * @class View1 is the "View" class in the MVC Pattern design and
 * implements the interface ViewI that offers flexibility to the design*/
public class View1 extends javax.swing.JFrame implements ViewI{
    //all the buttons that we are gonna add to the pane
    private JButton jButton0;
    private JButton jButton1;
    private JButton jButton2;
    private JButton jButton3;
    private JButton jButton4;
    private JButton jButton5;
    private JButton jButton6;
    private JButton jButton7;
    private JButton jButton8;
    private JButton jButton9;
    private JButton jButtonAdd;
    private JButton jButtonBackspace;
    private JButton jButtonC;
    private JButton jButtonDivide;
    private JButton jButtonEqual;
    private JButton jButtonMinus;
    private JButton jButtonMultiply;
    private JButton jButtonPlus;
    private JButton jButtonPower;
    private JButton jButtonSubstract;
    private JButton jButtonDerivate;
    private JButton jButtonIntegrate;
    private JButton jButtonX;
    private JTextField jTextDisplay;
    private JScrollPane scrollPane;
    private final static boolean shouldFill = true;



    public View1() {

        jTextDisplay = new javax.swing.JTextField();
        jButtonBackspace = new javax.swing.JButton();
        jButtonC = new javax.swing.JButton();
        jButton0 = new javax.swing.JButton();
        jButtonEqual = new javax.swing.JButton();
        jButtonPower = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButtonMinus = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButtonPlus = new javax.swing.JButton();
        jButtonX = new javax.swing.JButton();
        jButtonSubstract = new javax.swing.JButton();
        jButtonDerivate = new javax.swing.JButton();
        jButtonIntegrate = new javax.swing.JButton();
        jButtonAdd = new javax.swing.JButton();
        jButtonDivide = new javax.swing.JButton();
        jButtonMultiply = new javax.swing.JButton();

        //set the display to not be editable by the keyboard
        jTextDisplay.setEditable(false);
        //customizable settings
        jTextDisplay.setBackground(new java.awt.Color(245, 245, 245));
        jTextDisplay.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jTextDisplay.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextDisplay.setText("");
        jTextDisplay.setToolTipText("");


        //using a scroll pane in case the text output exceeds the display size
        scrollPane = new JScrollPane(jTextDisplay);

        //adding all the necessary listeners to the buttons
        jButtonBackspace.setText("<-");
        jButtonBackspace.addActionListener(evt -> jButtonBackspaceActionPerformed(evt));

        jButtonC.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jButtonC.setText("C");
        jButtonC.addActionListener(evt -> jButtonCActionPerformed(evt));

        jButton0.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jButton0.setText("0");
        jButton0.addActionListener(evt -> jButton0ActionPerformed(evt));

        jButtonEqual.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jButtonEqual.setText("=");


        jButtonPower.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jButtonPower.setText("^");
        jButtonPower.addActionListener(evt -> jButtonPowerActionPerformed(evt));

        jButton1.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jButton1.setText("1");
        jButton1.addActionListener(evt -> jButton1ActionPerformed(evt));

        jButton2.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jButton2.setText("2");
        jButton2.addActionListener(evt -> jButton2ActionPerformed(evt));

        jButton3.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jButton3.setText("3");
        jButton3.addActionListener(evt -> jButton3ActionPerformed(evt));

        jButtonMinus.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jButtonMinus.setText("-");
        jButtonMinus.addActionListener(evt -> jButtonMinusActionPerformed(evt));

        jButton4.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jButton4.setText("4");
        jButton4.addActionListener(evt -> jButton4ActionPerformed(evt));

        jButton5.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jButton5.setText("5");
        jButton5.addActionListener(evt -> jButton5ActionPerformed(evt));

        jButton6.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jButton6.setText("6");
        jButton6.addActionListener(evt -> jButton6ActionPerformed(evt));

        jButton7.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jButton7.setText("7");
        jButton7.addActionListener(evt -> jButton7ActionPerformed(evt));

        jButton8.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jButton8.setText("8");
        jButton8.addActionListener(evt -> jButton8ActionPerformed(evt));

        jButton9.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jButton9.setText("9");
        jButton9.addActionListener(evt -> jButton9ActionPerformed(evt));

        jButtonPlus.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jButtonPlus.setText("+");
        jButtonPlus.addActionListener(evt -> jButtonPlusActionPerformed(evt));

        jButtonX.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jButtonX.setText("x");
        jButtonX.addActionListener(evt -> jButtonXActionPerformed(evt));


        jButtonSubstract.setText("Substract");

        jButtonDerivate.setText("Derivate");

        jButtonIntegrate.setText("Integrate");

        jButtonAdd.setText("Add");

        jButtonDivide.setText("Divide");

        jButtonMultiply.setText("Multiply");

        JFrame frame = new JFrame("Polynomial Calculator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setResizable(false);
        frame.setType(java.awt.Window.Type.UTILITY);

        //Set up the content pane.
        addComponentsToPane(frame.getContentPane());

        //Display the window.
        frame.setVisible(true);
        frame.pack();

        scrollPane.setBackground(new java.awt.Color(200, 200, 200));

    }

    /***
     *method addComponentsToPane This method is used for adding all the elements
     * to the created pane by using the GridBagLayout for positioning
     * @param pane - Representing the container for our layout
     */

    private void addComponentsToPane(Container pane) {

        //set the desired layout
        pane.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        if (shouldFill) {
            //natural height, maximum width
            c.fill = GridBagConstraints.HORIZONTAL;
        }
        c.fill = GridBagConstraints.HORIZONTAL;
        c.ipadx = 80;
        c.ipady = 20;
        c.gridwidth = 4;// set the width of the component
        c.gridx = 0;//set column 0
        c.gridy = 0;//set row 0
        c.fill = GridBagConstraints.HORIZONTAL;
        pane.add(scrollPane, c);

        c.gridwidth = 1;
        c.gridx = 1;//set the column 1
        c.gridy = 1;// set the row 1
        pane.add(jButtonC, c);


        c.gridx = 0;
        c.gridy = 1;
        pane.add(jButtonBackspace, c);

        c.gridy = 2;
        c.gridx = 0;
        pane.add(jButton1, c);
        c.gridx = 1;
        pane.add(jButton2, c);
        c.gridx = 2;
        pane.add(jButton3, c);
        c.gridx = 3;
        pane.add(jButtonPlus, c);

        c.gridy = 3;
        c.gridx = 0;
        pane.add(jButton4, c);
        c.gridx = 1;
        pane.add(jButton5, c);
        c.gridx = 2;
        pane.add(jButton6, c);
        c.gridx = 3;
        pane.add(jButtonMinus, c);

        c.gridy = 4;
        c.gridx = 0;
        pane.add(jButton7, c);
        c.gridx = 1;
        pane.add(jButton8, c);
        c.gridx = 2;
        pane.add(jButton9, c);
        c.gridx = 3;
        pane.add(jButtonPower, c);


        c.gridy = 5;
        c.gridx = 0;
        pane.add(jButtonX, c);
        c.gridx = 1;
        pane.add(jButton0, c);
        c.gridwidth = 2;
        c.gridx = 2;
        pane.add(jButtonEqual, c);


        c.gridy = 6;
        c.gridx = 0;
        pane.add(jButtonAdd, c);
        c.gridx = 2;
        pane.add(jButtonSubstract, c);

        c.gridy = 7;
        c.gridx = 0;
        pane.add(jButtonMultiply, c);
        c.gridx = 2;
        pane.add(jButtonDivide, c);



        c.gridy = 8;
        c.gridx = 0;
        pane.add(jButtonDerivate, c);
        c.gridx = 2;
        pane.add(jButtonIntegrate, c);


    }



    private void jButtonCActionPerformed(java.awt.event.ActionEvent evt) {
        jTextDisplay.setText("");
    }

    private void jButtonBackspaceActionPerformed(java.awt.event.ActionEvent evt) {
        String str = jTextDisplay.getText();
        if (str != null && str.length() > 0) {
            str = str.substring(0, str.length() - 1);
        }
        jTextDisplay.setText(str);

    }

    private void jButton0ActionPerformed(java.awt.event.ActionEvent evt) {
        jTextDisplay.setText(jTextDisplay.getText() + "0");
    }

    private void jButtonPowerActionPerformed(java.awt.event.ActionEvent evt) {
        jTextDisplay.setText(jTextDisplay.getText() + "^");
    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
        jTextDisplay.setText(jTextDisplay.getText() + "1");
    }

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {
        jTextDisplay.setText(jTextDisplay.getText() + "2");
    }

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {
        jTextDisplay.setText(jTextDisplay.getText() + "3");
    }

    private void jButtonMinusActionPerformed(java.awt.event.ActionEvent evt) {
        jTextDisplay.setText(jTextDisplay.getText() + "-");
    }

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {
        jTextDisplay.setText(jTextDisplay.getText() + "4");
    }

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {
        jTextDisplay.setText(jTextDisplay.getText() + "5");
    }

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {
        jTextDisplay.setText(jTextDisplay.getText() + "6");
    }

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {
        jTextDisplay.setText(jTextDisplay.getText() + "7");
    }

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {
        jTextDisplay.setText(jTextDisplay.getText() + "8");
    }

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {
        jTextDisplay.setText(jTextDisplay.getText() + "9");
    }

    private void jButtonPlusActionPerformed(java.awt.event.ActionEvent evt) {
        jTextDisplay.setText(jTextDisplay.getText() + "+");
    }

    private void jButtonXActionPerformed(java.awt.event.ActionEvent evt) {
        jTextDisplay.setText(jTextDisplay.getText() + "x");
    }


    public String getText(){
        return jTextDisplay.getText();
    }

    public void substracteListener(ActionListener actionListener){
        jButtonSubstract.addActionListener(actionListener);
    };

    public void derivateListener(ActionListener actionListener){
        jButtonDerivate.addActionListener(actionListener);
    };

    public void integrateListener(ActionListener actionListener){
        jButtonIntegrate.addActionListener(actionListener);
    };


    public void equalListener(ActionListener actionListener){
        jButtonEqual.addActionListener(actionListener);
    };

    public void addListener(ActionListener actionListener){
        jButtonAdd.addActionListener(actionListener);
    };

    public void multiplyListener(ActionListener actionListener){
        jButtonMultiply.addActionListener(actionListener);
    };

    public void divideListener(ActionListener actionListener){
        jButtonDivide.addActionListener(actionListener);
    };

    // a method for displaying the errors
    public void displayErrorMessage(String errorMessage){

        JOptionPane.showMessageDialog(this, "ERROR "+ errorMessage);
    }
    public void setText(String string){
        jTextDisplay.setText(string);
    }


}
