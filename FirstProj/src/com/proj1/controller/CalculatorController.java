package com.proj1.controller;

import com.proj1.model.CalculatorModel;
import com.proj1.view.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * Calculator Controller - required components for MVC Pattern
 * It does the linking between the model and the view
 * Also adds to the view the necessary listeners
 */
public class CalculatorController {
    private enum State {IDLE,ADD, SUBSTRACT, MULTIPLY, DIVIDE, DERIVATE, INTEGRATE};

    private ViewI theView;
    private CalculatorModel theModel;
    private State theState;
    private String polynomial;

    public CalculatorController(ViewI theView, CalculatorModel theModel){
        this.theView = theView;
        this.theModel = theModel;
        this.theState= State.IDLE;

        this.theView.substracteListener(new SubstractListener());
        this.theView.addListener(new AddListener());
        this.theView.multiplyListener(new MultiplyListener());
        this.theView.divideListener(new DivideListener());
        this.theView.equalListener(new EqualListener());
        this.theView.derivateListener(new DerivateListener());
        this.theView.integrateListener(new IntegrateListener());
    }


    /**
     *Substract Listener - the listener for the substract button
     * Implements the actionPerformed method
     * */
    class SubstractListener implements ActionListener
    {
        public void actionPerformed(ActionEvent event){
            try {
                polynomial = theView.getText();
                theState = State.SUBSTRACT;
                theView.setText("");
                System.out.print(theState);


            }
            catch (Exception ex){
                //System.out.print(ex.getMessage());
                theView.displayErrorMessage(ex.getMessage());
            }
        }
    }

    /**
     *Derivate Listener - the listener for the Derivate button
     * Implements the actionPerformed method
     * */

    class DerivateListener implements ActionListener
    {
        public void actionPerformed(ActionEvent event){
            try {
                polynomial = theView.getText();
                theState = State.DERIVATE;
                theView.setText("");
                System.out.print(theState);


            }
            catch (Exception ex){
                //System.out.print(ex.getMessage());
                theView.displayErrorMessage(ex.getMessage());
            }
        }
    }


    /**
     *Integrate Listener - the listener for the Integrate button
     * Implements the actionPerformed method
     * */

    class IntegrateListener implements ActionListener
    {
        public void actionPerformed(ActionEvent event){
            try {
                polynomial = theView.getText();
                theState = State.INTEGRATE;
                theView.setText("");
                System.out.print(theState);


            }
            catch (Exception ex){
                //System.out.print(ex.getMessage());
                theView.displayErrorMessage(ex.getMessage());
            }
        }
    }

    /**
     *Add Listener - the listener for the Add button
     * Implements the actionPerformed method
     * */
    class AddListener implements ActionListener
    {
        public void actionPerformed(ActionEvent event){
            try {
                polynomial = theView.getText();
                theState = State.ADD;
                theView.setText("");
                System.out.print(theState);


            }
            catch (Exception ex){
                //System.out.print(ex.getMessage());
                theView.displayErrorMessage(ex.getMessage());
            }
        }
    }


    /**
     *Multiply Listener - the listener for the Multiply button
     * Implements the actionPerformed method
     * */
    class MultiplyListener implements ActionListener
    {
        public void actionPerformed(ActionEvent event){
            try {
                polynomial = theView.getText();
                theState = State.MULTIPLY;
                theView.setText("");
                System.out.print(theState);


            }
            catch (Exception ex){
                //System.out.print(ex.getMessage());
                theView.displayErrorMessage(ex.getMessage());
            }
        }
    }


    /**
     *Divide Listener - the listener for the divide button
     * Implements the actionPerformed method
     * */
    class DivideListener implements ActionListener
    {
        public void actionPerformed(ActionEvent event){
            try {
                polynomial = theView.getText();
                theState = State.DIVIDE;
                theView.setText("");
                System.out.print(theState);


            }
            catch (Exception ex){
                //System.out.print(ex.getMessage());
                theView.displayErrorMessage(ex.getMessage());
            }
        }
    }


    /**
     *Equal Listener - the listener for the Equal("=") button
     * Implements the actionPerformed method
     * */
    class EqualListener implements ActionListener
    {
        public void actionPerformed(ActionEvent event){
            try {
                String polynomial2 = theView.getText();
                if(polynomial2 != ""){
                    System.out.println(polynomial);
                    System.out.println(polynomial2);

                    switch(theState){
                        case SUBSTRACT:{
                           try{
                               String result = theModel.substractPolynomials(polynomial, polynomial2);
                               theView.setText(result);
                           }
                           catch(Exception e){
                               theView.displayErrorMessage(e.getMessage());
                           }
                        }
                            break;
                        case ADD:
                            try{
                                String result = theModel. addPolynomials(polynomial, polynomial2);
                                theView.setText(result);
                            }
                            catch(Exception e){
                                theView.displayErrorMessage(e.getMessage());
                            }
                            break;
                        case MULTIPLY:
                            try{
                                String result = theModel. multiplyPolynomials(polynomial, polynomial2);
                                theView.setText(result);
                            }
                            catch(Exception e){
                                theView.displayErrorMessage(e.getMessage());
                            }
                            break;
                        case DIVIDE:
                            try{
                                String result = theModel. dividePolynomials(polynomial, polynomial2);
                                theView.setText(result);
                            }
                            catch(Exception e){
                                theView.displayErrorMessage(e.getMessage());
                            }
                            break;
                        case DERIVATE:
                            try{
                                String result = theModel.derivatePolynomial(polynomial);
                                theView.setText(result);
                            }
                            catch(Exception e){
                                theView.displayErrorMessage(e.getMessage());
                            }
                            break;
                        case INTEGRATE:
                            try{
                                String result = theModel.integratePolynomial(polynomial);
                                theView.setText(result);
                            }
                            catch(Exception e){
                                theView.displayErrorMessage(e.getMessage());
                            }
                            break;

                        default:break;
                    }
                    theState = State.IDLE;
                }




            }
            catch (Exception ex){

                theView.displayErrorMessage(ex.getMessage());
            }
        }
    }






}
