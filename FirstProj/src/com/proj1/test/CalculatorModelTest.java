package com.proj1.test;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static com.proj1.model.CalculatorModel.*;
import static org.junit.jupiter.api.Assertions.*;

class CalculatorModelTest {
    @Test
    public void testSubstractPolynomials() throws Exception {
        String result = substractPolynomials("x^2", "x^3");
        Assert.assertEquals("-1x^3+1x^2", result);
        result = substractPolynomials("x^3+x^1", "x^5-x^3");
        Assert.assertEquals("-1x^5+2x^3+1x^1", result);
        result = substractPolynomials("x^4+x^5+1", "x^6+x^5+x^2-1");
        Assert.assertEquals("-1x^6+1x^4-1x^2+2 ", result);
    }

    @Test
    public void testAddPolynomials() throws Exception{
        String result = addPolynomials("x^2", "x^3");
        Assert.assertEquals("+1x^3+1x^2", result);
        result = addPolynomials("x^3+x^1", "x^5-x^3");
        Assert.assertEquals("+1x^5+1x^1", result);
        result = addPolynomials("x^4+x^5+1", "x^6+x^5+x^2-1");
        Assert.assertEquals("+1x^6+2x^5+1x^4+1x^2", result);
    }

    @Test
    public void testDividePolynomials() throws Exception{
        String result = dividePolynomials("x^2", "x^3");
        Assert.assertEquals("Quotient: | Reminder:+1x^2", result);

        result = dividePolynomials("x^6+x^1", "x^5-x^3");
        Assert.assertEquals("Quotient:+1x^1 | Reminder:+1x^4+1x^1", result);

        result = dividePolynomials("x^8+x^5+1", "x^6+x^5+x^2-1");
        Assert.assertEquals("Quotient:+1x^2 | Reminder:-1x^7+1x^5-1x^4+1x^2+1 ", result);
    }

    @Test
    public void testMultiplyPolynomials() throws Exception{
        String result = multiplyPolynomials("x^2", "x^3");
        Assert.assertEquals("+1x^5", result);
        result = multiplyPolynomials("x^3+x^1", "x^2-x^3");
        Assert.assertEquals("-1x^6+1x^5-1x^4+1x^3", result);
        result = multiplyPolynomials("x^4+x^5+1", "x^6+x^5+x^2-1");
        Assert.assertEquals("+1x^11+2x^10+1x^9+1x^7+2x^6-1x^4+1x^2-1 ", result);

    }

    @Test
    public void testDerivatePolynomials() throws Exception{
        String result = derivatePolynomial("x^2");
        Assert.assertEquals("+2x^1", result);
        result = derivatePolynomial("x^3+x^1");
        Assert.assertEquals("+3x^2+1 ", result);
        result = derivatePolynomial("x^4+x^5+x^3+1");
        Assert.assertEquals("+5x^4+4x^3+3x^2", result);

    }
    @Test
    public void testIntegratePolynomials() throws Exception{
        String result = integratePolynomial("x^2");
        Assert.assertEquals("+1/3x^3", result);
        result = integratePolynomial("x^3+x^1");
        Assert.assertEquals("+1/4x^4+1/2x^2", result);
        result = integratePolynomial("x^4+x^5+x^3+1");
        Assert.assertEquals("+1/6x^6+1/5x^5+1/4x^4+1x^1", result);

    }

}